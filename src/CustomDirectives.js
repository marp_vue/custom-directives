export default {
    bind(el, binding, vnode){
        let delay = 0;
        if(binding.modifiers['delay']){
            delay = 3000;
        }
        let timeout = setTimeout(() => {
            if(binding.arg == 'background'){
                el.style.backgroundColor = binding.value;
            }
            if(binding.arg == 'text'){
                el.style.color = binding.value;
            }    
        }, delay);
    }
}